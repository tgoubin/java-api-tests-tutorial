package com.goubinthibault.apiteststutorial.tests.api.personne;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.goubinthibault.apiteststutorial.api.dto.Personne;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;
import com.goubinthibault.apiteststutorial.api.personne.PersonneMapper;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestUtils;

/**
 * Tests API pour PUT /personne/{id}
 * 
 * @author Thibault GOUBIN
 */
public class PutPersonneApiTest extends PersonneApiTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PutPersonneApiTest.class);

	private static final String REQUESTS_FOLDER = PersonneApiTestConstants.PUT_PERSONNE_REQUESTS_FOLDER
			+ "putPersonneApiTest/";

	/**
	 * PUT /personne/{id} - cas nominal
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void putPersonne_200() throws Exception {
		LOGGER.info("PUT /personne/{id} - cas nominal");

		// Definition de l'objet Personne attendu en reponse
		Personne personne = PersonneMapper.entityToDto(
				new PersonneEntity(2, "TEST", "Test", "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.FEMME));

		// Appel de PUT /personne/{id} et verification du contenu de la reponse
		PersonneApiTestUtils.putPersonneOK(mockMvc, resourceLoader, REQUESTS_FOLDER + "putPersonne_200", personne);

		// Verification - par un GET /personne/{id} - qu'elle est accessible
		PersonneApiTestUtils.getPersonneByIdOK(mockMvc, personne);

		// Verification - par un GET /personne - que le jeu de donnees contient 5
		// Personnes (autant de personnes)
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, 5);
	}

	/**
	 * PUT /personne/{id} - cas d'erreur - la regle n'existe pas
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void putPersonne_404() throws Exception {
		LOGGER.info("PUT /personne/{id} - cas d'erreur - la regle n'existe pas");

		// Appel de PUT /personne/{id} avec un identifiant inconnu
		Integer id = 10;
		ResultActions resultActions = PersonneApiTestUtils.putPersonne(mockMvc, resourceLoader,
				REQUESTS_FOLDER + "putPersonne_404", id);

		// Verification que la reponse est une erreur 404
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isNotFound());

		// Verification du message d'erreur
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(),
				"Il n'y a aucune Personne avec l'identifiant '" + id + "'");
	}

	/**
	 * PUT /personne/{id} - cas d'erreur - les identifiants ne correspondent pas
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void putPersonne_400_IdsNeCorrespondentPas() throws Exception {
		LOGGER.info("PUT /personne/{id} - cas d'erreur - les identifiants ne correspondent pas");

		// Test avec un identifiant qui ne correspond pas
		putPersonne_400_IdsNeCorrespondentPas("putPersonne_400_id_ne_correspond_pas",
				PersonneApiTestConstants.PERSONNE_3);

		// Test avec un identifiant manquant
		putPersonne_400_IdsNeCorrespondentPas("putPersonne_400_id_manquant", PersonneApiTestConstants.PERSONNE_4);
	}

	/**
	 * PUT /personne/{id} - cas d'erreur - les identifiants ne correspondent pas
	 * 
	 * @param requestFile le fichier de requete JSON
	 * @param personne    la Personne
	 * @throws Exception une exception
	 */
	private void putPersonne_400_IdsNeCorrespondentPas(String requestFile, Personne personne) throws Exception {
		// Appel de PUT /personne/{id}
		ResultActions resultActions = PersonneApiTestUtils.putPersonne(mockMvc, resourceLoader,
				REQUESTS_FOLDER + requestFile, personne.getId());

		// Verification que la reponse est une erreur 400
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

		// Verification du message d'erreur
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(),
				"Les deux identifiants ne correspondent pas");

		// Verification - par un GET /personne/{id} - que la Personne n'a pas ete
		// modifiee
		PersonneApiTestUtils.getPersonneByIdOK(mockMvc, personne);
	}

	/**
	 * PUT /personne/{id} - cas d'erreur - 400 - exceptions Spring
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void putPersonne_400_SpringBadRequests() throws Exception {
		LOGGER.info("PUT /personne/{id} - cas d'erreur - 400 - exceptions Spring");

		// Personne initiale
		Personne personneInitiale = PersonneApiTestConstants.PERSONNE_3;

		// Test avec un sexe invalide
		putPersonne_400_SpringBadRequest("putPersonne_400_sexe_invalide", HttpMessageNotReadableException.class,
				"Unexpected value 'SEXE_INVALIDE'", personneInitiale);

		// Test avec une date de naissance invalide
		putPersonne_400_SpringBadRequest("putPersonne_400_date_naissance_invalide",
				HttpMessageNotReadableException.class, "Text 'DATE_INVALIDE' could not be parsed", personneInitiale);
	}

	/**
	 * PUT /personne/{id} - cas d'erreur - 400 - exception Spring
	 * 
	 * @param requestFile      le fichier de requete JSON
	 * @param exceptionClass   la classe d'exception
	 * @param errorMessage     la portion du message d'erreur attendu
	 * @param personneInitiale la Personne initiale
	 * @throws Exception une exception
	 */
	private void putPersonne_400_SpringBadRequest(String requestFile, Class<? extends Exception> exceptionClass,
			String errorMessage, Personne personneInitiale) throws Exception {
		// Appel de PUT /personne/{id}
		ResultActions resultActions = PersonneApiTestUtils.putPersonne(mockMvc, resourceLoader,
				REQUESTS_FOLDER + requestFile, personneInitiale.getId());

		// Verification que la reponse est une erreur 400
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

		// Verification de l'erreur renvoyee par le framework Spring
		ApiTestUtils.verifySpringBadRequest(resultActions, exceptionClass, errorMessage);

		// Verification - par un GET /personne/{id} - que la Personne n'a pas ete
		// modifiee
		PersonneApiTestUtils.getPersonneByIdOK(mockMvc, personneInitiale);
	}

	/**
	 * PUT /personne/{id} - cas d'erreur - 400 - elements obligatoires
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void putPersonne_400_MandatoryValues() throws Exception {
		LOGGER.info("PUT /personne/{id} - cas d'erreur - 400 - elements obligatoires");

		// Personne initiale
		Personne personneInitiale = PersonneApiTestConstants.PERSONNE_2;

		// Test avec un nom manquant / vide
		putPersonne_400_MandatoryValue("putPersonne_400_nom_manquant", "nom", personneInitiale);
		putPersonne_400_MandatoryValue("putPersonne_400_nom_vide", "nom", personneInitiale);

		// Test avec un prenom manquant / vide
		putPersonne_400_MandatoryValue("putPersonne_400_prenom_manquant", "prenom", personneInitiale);
		putPersonne_400_MandatoryValue("putPersonne_400_prenom_vide", "prenom", personneInitiale);

		// Test avec une date de naissance manquante
		putPersonne_400_MandatoryValue("putPersonne_400_date_naissance_manquante", "dateNaissance", personneInitiale);

		// Test avec un sexe manquant
		putPersonne_400_MandatoryValue("putPersonne_400_sexe_manquant", "sexe", personneInitiale);
	}

	/**
	 * PUT /personne/{id} - cas d'erreur - 400 - element obligatoire
	 * 
	 * @param requestFile      le fichier de requete JSON
	 * @param nomChamp         le nom du champ obligatoire
	 * @param personneInitiale la Personne initiale
	 * @throws Exception une exception
	 */
	private void putPersonne_400_MandatoryValue(String requestFile, String nomChamp, Personne personneInitiale)
			throws Exception {
		// Appel de PUT /personne/{id} avec un email invalide
		ResultActions resultActions = PersonneApiTestUtils.putPersonne(mockMvc, resourceLoader,
				REQUESTS_FOLDER + requestFile, personneInitiale.getId());

		// Verification que la reponse est une erreur 400
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

		// Verification du message d'erreur
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(),
				"Le champ '" + nomChamp + "' est obligatoire");

		// Verification - par un GET /personne/{id} - que la Personne n'a pas ete
		// modifiee
		PersonneApiTestUtils.getPersonneByIdOK(mockMvc, personneInitiale);
	}

	/**
	 * PUT /personne/{id} - cas d'erreur - 400 - email invalide
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void putPersonne_400_EmailInvalide() throws Exception {
		LOGGER.info("PUT /personne/{id} - cas d'erreur - 400 - email invalide");

		// Appel de PUT /personne/{id} avec un email invalide
		ResultActions resultActions = PersonneApiTestUtils.putPersonne(mockMvc, resourceLoader,
				REQUESTS_FOLDER + "putPersonne_400_email_invalide", 2);

		// Verification que la reponse est une erreur 400
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

		// Verification du message d'erreur
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(),
				"L'email 'test@gmail.' n'est pas valide");

		// Verification - par un GET /personne/{id} - que la Personne n'a pas ete
		// modifiee
		PersonneApiTestUtils.getPersonneByIdOK(mockMvc, PersonneApiTestConstants.PERSONNE_2);
	}
}
