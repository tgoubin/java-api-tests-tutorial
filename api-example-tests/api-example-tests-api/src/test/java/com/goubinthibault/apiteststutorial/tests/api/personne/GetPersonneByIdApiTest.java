package com.goubinthibault.apiteststutorial.tests.api.personne;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Tests API pour GET /personne/{id}
 * 
 * @author Thibault GOUBIN
 */
public class GetPersonneByIdApiTest extends PersonneApiTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetPersonneByIdApiTest.class);

	/**
	 * GET /personne/{id} - cas nominal
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonneById_200() throws Exception {
		LOGGER.info("GET /personne/{id} - cas nominal");

		// Appel de GET /personne/{id} et verification du contenu de la reponse
		PersonneApiTestUtils.getPersonneByIdOK(mockMvc, PersonneApiTestConstants.PERSONNE_4);
	}

	/**
	 * GET /personne/{id} - cas d'erreur - la regle n'existe pas
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonneById_404() throws Exception {
		LOGGER.info("GET /personne/{id} - cas d'erreur - la regle n'existe pas");

		// Appel de GET /personne/{id} avec un identifiant inconnu
		Integer id = 10;
		ResultActions resultActions = PersonneApiTestUtils.getPersonneById(mockMvc, id);

		// Verification que la reponse est une erreur 404
		resultActions.andExpect(MockMvcResultMatchers.status().isNotFound());

		// Verification du message d'erreur
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(),
				"Il n'y a aucune Personne avec l'identifiant '" + id + "'");
	}
}
