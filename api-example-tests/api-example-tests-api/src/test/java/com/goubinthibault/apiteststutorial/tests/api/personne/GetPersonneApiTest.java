package com.goubinthibault.apiteststutorial.tests.api.personne;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.goubinthibault.apiteststutorial.api.dto.Personne.SexeEnum;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestUtils;

/**
 * Tests API pour GET /personne
 * 
 * @author Thibault GOUBIN
 */
public class GetPersonneApiTest extends PersonneApiTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetPersonneApiTest.class);

	/**
	 * GET /personne - cas nominal - scenario 1
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonne_200_1() throws Exception {
		LOGGER.info("GET /personne - cas nominal - scenario 1");

		// Appel de GET /personne (recherche par nom) et verification du contenu de la
		// reponse
		PersonneApiTestUtils.getPersonneOK(mockMvc, "BRYANT", null, null, 1);
	}

	/**
	 * GET /personne - cas nominal - scenario 2
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonne_200_2() throws Exception {
		LOGGER.info("GET /personne - cas nominal - scenario 2");

		// Appel de GET /personne (recherche par prenom) et verification du contenu de
		// la
		// reponse
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, "Zinedine", null, 1);
	}

	/**
	 * GET /personne - cas nominal - scenario 3
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonne_200_3() throws Exception {
		LOGGER.info("GET /personne - cas nominal - scenario 3");

		// Appel de GET /personne (recherche par sexe) et verification du contenu de la
		// reponse
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, SexeEnum.FEMME.name(), 2);
	}

	/**
	 * GET /personne - cas nominal - scenario 4
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonne_200_4() throws Exception {
		LOGGER.info("GET /personne - cas nominal - scenario 4");

		// Appel de GET /personne (recherche par nom, prenom et sexe) et verification du
		// contenu de la reponse
		PersonneApiTestUtils.getPersonneOK(mockMvc, "ROBERTS", "Julia", SexeEnum.FEMME.name(), 1);
	}

	/**
	 * GET /personne - cas nominal - scenario 5
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonne_200_5() throws Exception {
		LOGGER.info("GET /personne - cas nominal - scenario 5");

		// Appel de GET /personne (recherche par nom, prenom et sexe) et verification du
		// contenu de la reponse
		PersonneApiTestUtils.getPersonneOK(mockMvc, "ROBERTS", "Julia", SexeEnum.HOMME.name(), 0);
	}

	/**
	 * GET /personne - cas nominal - scenario 6
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonne_200_6() throws Exception {
		LOGGER.info("GET /personne - cas nominal - scenario 5");

		// Appel de GET /personne et verification du contenu de la reponse
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, 5);
	}

	/**
	 * GET /personne - cas d'erreur - 400 - sexe invalide
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void getPersonne_400() throws Exception {
		LOGGER.info("GET /personne - cas d'erreur - 400 - sexe invalide");

		// Appel de GET /personne avec un sexe invalide
		ResultActions resultActions = PersonneApiTestUtils.getPersonne(mockMvc, null, null, "SEXE_INVALIDE");

		// Verification que la reponse est une erreur 400
		resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

		// Verification de l'erreur renvoyee par le framework Spring
		ApiTestUtils.verifySpringBadRequest(resultActions, MethodArgumentTypeMismatchException.class,
				"for value 'SEXE_INVALIDE'");
	}
}
