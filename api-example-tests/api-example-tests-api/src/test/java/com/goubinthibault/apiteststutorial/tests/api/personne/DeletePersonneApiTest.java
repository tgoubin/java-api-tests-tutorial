package com.goubinthibault.apiteststutorial.tests.api.personne;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Tests API pour DELETE /personne/{id}
 * 
 * @author Thibault GOUBIN
 */
public class DeletePersonneApiTest extends PersonneApiTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeletePersonneApiTest.class);

	/**
	 * DELETE /personne/{id} - cas nominal
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void deletePersonne_204() throws Exception {
		LOGGER.info("DELETE /personne/{id} - cas nominal");

		// Appel de DELETE /personne/{id} et verification du contenu de la reponse
		Integer id = 2;
		PersonneApiTestUtils.deletePersonneOK(mockMvc, id);

		// Verification - par un GET /personne/{id} - qu'elle n'est plus accessible
		PersonneApiTestUtils.getPersonneById(mockMvc, id).andExpect(MockMvcResultMatchers.status().isNotFound());

		// Verification - par un GET /personne - que le jeu de donnees contient 4
		// Personnes (une personne en moins)
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, 4);
	}
	
	/**
	 * DELETE /personne/{id} - cas d'erreur - la regle n'existe pas
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void deletePersonne_404() throws Exception {
		LOGGER.info("DELETE /personne/{id} - cas d'erreur - la regle n'existe pas");

		// Appel de DELETE /personne/{id} avec un identifiant inconnu
		Integer id = 10;
		ResultActions resultActions = PersonneApiTestUtils.deletePersonne(mockMvc, id);

		// Verification que la reponse est une erreur 404
		resultActions.andExpect(MockMvcResultMatchers.status().isNotFound());

		// Verification du message d'erreur
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(),
				"Il n'y a aucune Personne avec l'identifiant '" + id + "'");

		// Verification - par un GET /personne - que le jeu de donnees contient 5
		// Personnes (aucune personne en moins)
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, 5);
	}
}
