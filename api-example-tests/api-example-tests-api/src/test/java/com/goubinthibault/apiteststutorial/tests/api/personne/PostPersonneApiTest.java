package com.goubinthibault.apiteststutorial.tests.api.personne;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.goubinthibault.apiteststutorial.api.dto.Personne;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;
import com.goubinthibault.apiteststutorial.api.personne.PersonneMapper;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestUtils;

/**
 * Tests API pour POST /personne
 * 
 * @author Thibault GOUBIN
 */
public class PostPersonneApiTest extends PersonneApiTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PostPersonneApiTest.class);

	private static final String REQUESTS_FOLDER = PersonneApiTestConstants.POST_PERSONNE_REQUESTS_FOLDER
			+ "postPersonneApiTest/";

	/**
	 * POST /personne - cas nominal
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void postPersonne_201() throws Exception {
		LOGGER.info("POST /personne - cas nominal");

		// Definition de l'objet Personne attendu en reponse
		Personne personne = PersonneMapper.entityToDto(
				new PersonneEntity(6, "TEST", "Test", "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));

		// Appel de POST /personne et verification du contenu de la reponse
		PersonneApiTestUtils.postPersonneOK(mockMvc, resourceLoader, REQUESTS_FOLDER + "postPersonne_201", personne);

		// Verification - par un GET /personne/{id} - qu'elle est accessible
		PersonneApiTestUtils.getPersonneByIdOK(mockMvc, personne);

		// Verification - par un GET /personne - que le jeu de donnees contient 6
		// Personnes (une personne de plus)
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, 6);
	}

	/**
	 * POST /personne - cas d'erreur - 400 - exceptions Spring
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void postPersonne_400_SpringBadRequests() throws Exception {
		LOGGER.info("POST /personne - cas d'erreur - 400 - exceptions Spring");

		// Test avec un sexe invalide
		postPersonne_400_SpringBadRequest("postPersonne_400_sexe_invalide", HttpMessageNotReadableException.class,
				"Unexpected value 'SEXE_INVALIDE'");

		// Test avec une date de naissance invalide
		postPersonne_400_SpringBadRequest("postPersonne_400_date_naissance_invalide",
				HttpMessageNotReadableException.class, "Text 'DATE_INVALIDE' could not be parsed");
	}

	/**
	 * POST /personne - cas d'erreur - 400 - exception Spring
	 * 
	 * @param requestFile    le fichier de requete JSON
	 * @param exceptionClass la classe d'exception
	 * @param errorMessage   la portion du message d'erreur attendu
	 * @throws Exception une exception
	 */
	private void postPersonne_400_SpringBadRequest(String requestFile, Class<? extends Exception> exceptionClass,
			String errorMessage) throws Exception {
		// Appel de POST /personne
		ResultActions resultActions = PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader,
				REQUESTS_FOLDER + requestFile);

		// Verification que la reponse est une erreur 400
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

		// Verification de l'erreur renvoyee par le framework Spring
		ApiTestUtils.verifySpringBadRequest(resultActions, exceptionClass, errorMessage);

		// Verification - par un GET /personne - que le jeu de donnees contient 5
		// Personnes (aucune personne en plus)
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, 5);
	}

	/**
	 * POST /personne - cas d'erreur - 400 - elements obligatoires
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void postPersonne_400_MandatoryValues() throws Exception {
		LOGGER.info("POST /personne - cas d'erreur - 400 - elements obligatoires");

		// Test avec un nom manquant / vide
		postPersonne_400_MandatoryValue("postPersonne_400_nom_manquant", "nom");
		postPersonne_400_MandatoryValue("postPersonne_400_nom_vide", "nom");

		// Test avec un prenom manquant / vide
		postPersonne_400_MandatoryValue("postPersonne_400_prenom_manquant", "prenom");
		postPersonne_400_MandatoryValue("postPersonne_400_prenom_vide", "prenom");

		// Test avec une date de naissance manquante
		postPersonne_400_MandatoryValue("postPersonne_400_date_naissance_manquante", "dateNaissance");

		// Test avec un sexe manquant
		postPersonne_400_MandatoryValue("postPersonne_400_sexe_manquant", "sexe");
	}

	/**
	 * POST /personne - cas d'erreur - 400 - element obligatoire
	 * 
	 * @param requestFile le fichier de requete JSON
	 * @param nomChamp    le nom du champ obligatoire
	 * @throws Exception une exception
	 */
	private void postPersonne_400_MandatoryValue(String requestFile, String nomChamp) throws Exception {
		// Appel de POST /personne avec un email invalide
		ResultActions resultActions = PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader,
				REQUESTS_FOLDER + requestFile);

		// Verification que la reponse est une erreur 400
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

		// Verification du message d'erreur
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(),
				"Le champ '" + nomChamp + "' est obligatoire");

		// Verification - par un GET /personne - que le jeu de donnees contient 5
		// Personnes (aucune personne en plus)
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, 5);
	}

	/**
	 * POST /personne - cas d'erreur - 400 - email invalide
	 * 
	 * @throws Exception une exception
	 */
	@Test
	public void postPersonne_400_EmailInvalide() throws Exception {
		LOGGER.info("POST /personne - cas d'erreur - 400 - email invalide");

		// Appel de POST /personne avec un email invalide
		ResultActions resultActions = PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader,
				REQUESTS_FOLDER + "postPersonne_400_email_invalide");

		// Verification que la reponse est une erreur 400
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());

		// Verification du message d'erreur
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(),
				"L'email 'test@gmail.' n'est pas valide");

		// Verification - par un GET /personne - que le jeu de donnees contient 5
		// Personnes (aucune personne en plus)
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, 5);
	}
}
