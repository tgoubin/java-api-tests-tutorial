package com.goubinthibault.apiteststutorial.tests.api.personne;

import java.time.LocalDate;

import com.goubinthibault.apiteststutorial.api.dto.Personne;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity;
import com.goubinthibault.apiteststutorial.api.personne.PersonneMapper;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestConstants;

/**
 * Constantes pour les tests API sur Personne
 * 
 * @author Thibault GOUBIN
 */
public class PersonneApiTestConstants {

	public static final String PERSONNE_PATH = "/personne";

	public static final String POST_PERSONNE_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER + PERSONNE_PATH
			+ "/post/";
	public static final String PUT_PERSONNE_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER + PERSONNE_PATH
			+ "/put/";

	public static final Personne PERSONNE_2 = PersonneMapper.entityToDto(
			new PersonneEntity(2, "BRYANT", "Kobe", "kobe824@orange.fr", LocalDate.of(1978, 3, 4), SexeEnum.HOMME));
	public static final Personne PERSONNE_3 = PersonneMapper.entityToDto(new PersonneEntity(3, "ROBERTS", "Julia",
			"roberts.julia@gmail.com", LocalDate.of(1965, 4, 6), SexeEnum.FEMME));
	public static final Personne PERSONNE_4 = PersonneMapper.entityToDto(
			new PersonneEntity(4, "ZIDANE", "Zinedine", "zizou@hotmail.fr", LocalDate.of(1972, 5, 8), SexeEnum.HOMME));
}
