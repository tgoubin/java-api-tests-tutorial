package com.goubinthibault.apiteststutorial.tests.api.personne;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.goubinthibault.apiteststutorial.api.dto.Personne;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestConstants;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestUtils;

/**
 * Methodes utilitaires pour les tests API sur Personne
 * 
 * @author Thibault GOUBIN
 */
public class PersonneApiTestUtils {

	/**
	 * Execution d'un GET /personne
	 * 
	 * @param mockMvc l'objet MockMvc
	 * @param nom     le nom
	 * @param prenom  le prenom
	 * @param sexe    le sexe
	 * @return l'objet ResultActions
	 * @throws Exception une exception
	 */
	public static ResultActions getPersonne(MockMvc mockMvc, String nom, String prenom, String sexe) throws Exception {
		StringBuilder paramsString = new StringBuilder("?");

		if (nom != null && !"".equals(nom.trim())) {
			paramsString.append("nom=").append(nom.trim()).append("&");
		}

		if (prenom != null && !"".equals(prenom.trim())) {
			paramsString.append("prenom=").append(prenom.trim()).append("&");
		}

		if (sexe != null && !"".equals(sexe.trim())) {
			paramsString.append("sexe=" + sexe.trim());
		}

		return mockMvc
				.perform(MockMvcRequestBuilders.get(PersonneApiTestConstants.PERSONNE_PATH + paramsString.toString())
						.contentType(MediaType.APPLICATION_JSON));
	}

	/**
	 * Execution d'un GET /personne et verification du succes de la requete
	 * 
	 * @param mockMvc  l'objet MockMvc
	 * @param personne l'objet Personne attendu
	 * @throws Exception une exception
	 */
	@SuppressWarnings("unchecked")
	public static void getPersonneOK(MockMvc mockMvc, String nom, String prenom, String sexe, int nbResultsExpected)
			throws Exception {
		ResultActions resultActions = getPersonne(mockMvc, nom, prenom, sexe)
				.andExpect(MockMvcResultMatchers.status().isOk());
		ApiTestUtils.logResponse(resultActions);

		// Verification que la reponse contient bien le nombre d'elements attendus
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(nbResultsExpected)));

		// Verification du contenu de chaque element de la reponse
		String responseString = resultActions.andReturn().getResponse().getContentAsString();
		List<Map<String, String>> response = ApiTestConstants.JSON_MAPPER.readValue(responseString, List.class);

		// Verification que chaque personne repond bien aux parametres de recherche
		for (int i = 0; i < response.size(); i++) {
			Personne personne = ApiTestConstants.JSON_MAPPER
					.readValue(ApiTestConstants.JSON_MAPPER.writeValueAsString(response.get(i)), Personne.class);

			if (nom != null && !"".equals(nom.trim())) {
				Assert.assertEquals(nom, personne.getNom());
			}

			if (prenom != null && !"".equals(prenom.trim())) {
				Assert.assertEquals(prenom, personne.getPrenom());
			}

			if (sexe != null) {
				Assert.assertEquals(sexe, personne.getSexe().name());
			}
		}
	}

	/**
	 * Execution d'un GET /personne/{id}
	 * 
	 * @param mockMvc l'objet MockMvc
	 * @param id      l'identifiant de la personne
	 * @return l'objet ResultActions
	 * @throws Exception une exception
	 */
	public static ResultActions getPersonneById(MockMvc mockMvc, Integer id) throws Exception {
		ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
				.get(PersonneApiTestConstants.PERSONNE_PATH + "/" + id).contentType(MediaType.APPLICATION_JSON));
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}

	/**
	 * Execution d'un GET /personne/{id} et verification du succes de la requete
	 * 
	 * @param mockMvc  l'objet MockMvc
	 * @param personne l'objet Personne attendu
	 * @throws Exception une exception
	 */
	public static void getPersonneByIdOK(MockMvc mockMvc, Personne personne) throws Exception {
		ResultActions resultActions = getPersonneById(mockMvc, personne.getId())
				.andExpect(MockMvcResultMatchers.status().isOk());
		verifyPersonneContent(resultActions, personne);
	}

	/**
	 * Execution d'un POST /personne
	 * 
	 * @param mockMvc        l'objet MockMvc
	 * @param resourceLoader l'objet ResourceLoader
	 * @param requestFile    le fichier de requete JSON
	 * @return l'objet ResultActions
	 * @throws Exception une exception
	 */
	public static ResultActions postPersonne(MockMvc mockMvc, ResourceLoader resourceLoader, String requestFile)
			throws Exception {
		String jsonRequest = ApiTestUtils.getRequestFileContent(resourceLoader, requestFile);
		ResultActions resultActions = mockMvc
				.perform(MockMvcRequestBuilders.post(PersonneApiTestConstants.PERSONNE_PATH)
						.contentType(MediaType.APPLICATION_JSON).content(jsonRequest));
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}

	/**
	 * Execution d'un POST /personne et verification du succes de la requete
	 * 
	 * @param mockMvc        l'objet MockMvc
	 * @param resourceLoader l'objet ResourceLoader
	 * @param requestFile    le fichier de requete JSON
	 * @param personne       l'objet Personne attendu
	 * @throws Exception une exception
	 */
	public static void postPersonneOK(MockMvc mockMvc, ResourceLoader resourceLoader, String requestFile,
			Personne personne) throws Exception {
		ResultActions resultActions = postPersonne(mockMvc, resourceLoader, requestFile)
				.andExpect(MockMvcResultMatchers.status().isCreated());
		verifyPersonneContent(resultActions, personne);
	}

	/**
	 * Execution d'un DELETE /personne/{id}
	 * 
	 * @param mockMvc        l'objet MockMvc
	 * @param resourceLoader l'objet ResourceLoader
	 * @param requestFile    le fichier de requete JSON
	 * @param id             l'identifiant de la personne
	 * @return l'objet ResultActions
	 * @throws Exception une exception
	 */
	public static ResultActions putPersonne(MockMvc mockMvc, ResourceLoader resourceLoader, String requestFile,
			Integer id) throws Exception {
		String jsonRequest = ApiTestUtils.getRequestFileContent(resourceLoader, requestFile);
		ResultActions resultActions = mockMvc
				.perform(MockMvcRequestBuilders.put(PersonneApiTestConstants.PERSONNE_PATH + "/" + id)
						.contentType(MediaType.APPLICATION_JSON).content(jsonRequest));
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}

	/**
	 * Execution d'un PUT /personne/{id} et verification du succes de la requete
	 * 
	 * @param mockMvc        l'objet MockMvc
	 * @param resourceLoader l'objet ResourceLoader
	 * @param requestFile    le fichier de requete JSON
	 * @param personne       l'objet Personne attendu
	 * @throws Exception une exception
	 */
	public static void putPersonneOK(MockMvc mockMvc, ResourceLoader resourceLoader, String requestFile,
			Personne personne) throws Exception {
		ResultActions resultActions = putPersonne(mockMvc, resourceLoader, requestFile, personne.getId())
				.andExpect(MockMvcResultMatchers.status().isOk());
		verifyPersonneContent(resultActions, personne);
	}

	/**
	 * Execution d'un DELETE /personne/{id}
	 * 
	 * @param mockMvc l'objet MockMvc
	 * @param id      l'identifiant de la personne
	 * @return l'objet ResultActions
	 * @throws Exception une exception
	 */
	public static ResultActions deletePersonne(MockMvc mockMvc, Integer id) throws Exception {
		ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
				.delete(PersonneApiTestConstants.PERSONNE_PATH + "/" + id).contentType(MediaType.APPLICATION_JSON));
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}

	/**
	 * Execution d'un DELETE /personne/{id} et verification du succes de la requete
	 * 
	 * @param mockMvc l'objet MockMvc
	 * @param id      l'identifiant de la personne
	 * @throws Exception une exception
	 */
	public static void deletePersonneOK(MockMvc mockMvc, Integer id) throws Exception {
		deletePersonne(mockMvc, id).andExpect(MockMvcResultMatchers.status().isNoContent());
	}

	/**
	 * Verification du contenu d'un resultat de requete par rapport a un objet
	 * Personne
	 * 
	 * @param resultActions l'objet ResultActions
	 * @param personne      l'objet Personne attendu
	 * @throws Exception une exception
	 */
	public static void verifyPersonneContent(ResultActions resultActions, Personne personne) throws Exception {
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(personne.getId()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.nom").value(personne.getNom()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.prenom").value(personne.getPrenom()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.email").value(personne.getEmail()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.dateNaissance")
						.value(personne.getDateNaissance().format(ApiTestConstants.DATE_FORMATTER)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.sexe").value(personne.getSexe().name()));
	}
}
