package com.goubinthibault.apiteststutorial.tests.api.personne;

import java.util.HashSet;

import org.junit.Ignore;

import com.goubinthibault.apiteststutorial.api.personne.PersonneFakeRepository;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTest;

/**
 * Classe abstraite commune pour les tests API sur Personne
 * 
 * @author Thibault GOUBIN
 */
@Ignore
public class PersonneApiTest extends ApiTest {

	private static final String INIT_DATA_REQUESTS_FOLDER = PersonneApiTestConstants.POST_PERSONNE_REQUESTS_FOLDER
			+ "personneApiTest/";

	@Override
	public void initData() throws Exception {
		PersonneFakeRepository.PERSONNES = new HashSet<>();
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_1");
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_2");
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_3");
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_4");
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_5");
	}
}
