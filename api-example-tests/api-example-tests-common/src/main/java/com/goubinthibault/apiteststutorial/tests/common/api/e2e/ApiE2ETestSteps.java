package com.goubinthibault.apiteststutorial.tests.common.api.e2e;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.web.servlet.MockMvc;

import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestUtils;

/**
 * Classe abstraite commune pour les steps API E2E
 * 
 * @author Thibault GOUBIN
 */
public abstract class ApiE2ETestSteps {

	/**
	 * Le moteur des tests API
	 */
	@Autowired
	protected MockMvc mockMvc;

	@Autowired
	protected ResourceLoader resourceLoader;

	/**
	 * Initialisation du mapper JSON
	 * 
	 * @throws Exception une exception
	 */
	public void init() throws Exception {
		ApiTestUtils.initJSONMapper();
	}
}
