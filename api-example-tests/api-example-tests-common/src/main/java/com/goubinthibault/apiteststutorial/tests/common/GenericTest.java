package com.goubinthibault.apiteststutorial.tests.common;

import org.junit.Before;

/**
 * Classe abstraite commune pour les tests
 * 
 * @author Thibault GOUBIN
 */
public abstract class GenericTest {

	/**
	 * Initialisation du moteur de tests et des donnees
	 * 
	 * @throws Exception une exception
	 */
	@Before
	public abstract void init() throws Exception;

	/**
	 * Initialisation des donnees
	 * 
	 * @throws Exception une exception
	 */
	public void initData() throws Exception {
		// Nothing to do
	}
}
