package com.goubinthibault.apiteststutorial.tests.common.api;

import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Constantes pour les tests API
 * 
 * @author Thibault GOUBIN
 */
public class ApiTestConstants {

	public static final String REQUESTS_FOLDER = "file:src/test/resources/requetes";

	public static final String REQUEST_FILE_EXTENSION = ".json";

	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ISO_DATE;

	public static final ObjectMapper JSON_MAPPER = new ObjectMapper();
}
