package com.goubinthibault.apiteststutorial.tests.common.api;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.goubinthibault.apiteststutorial.api.ApiExampleRestApplication;
import com.goubinthibault.apiteststutorial.tests.common.GenericTest;

/**
 * Classe abstraite commune pour les tests API
 * 
 * @author Thibault GOUBIN
 */
@SpringBootTest(classes = {
		ApiExampleRestApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public abstract class ApiTest extends GenericTest {

	/**
	 * Le moteur des tests API
	 */
	@Autowired
	protected MockMvc mockMvc;

	@Autowired
	protected ResourceLoader resourceLoader;

	/**
	 * Initialisation du mapper JSON et des donnees
	 * 
	 * @throws Exception une exception
	 */
	@Override
	public void init() throws Exception {
		ApiTestUtils.initJSONMapper();
		initData();
	}
}
