package com.goubinthibault.apiteststutorial.tests.common.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

/**
 * Methodes utilitaires pour les tests API
 * 
 * @author Thibault GOUBIN
 */
@SuppressWarnings("deprecation")
public class ApiTestUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApiTestUtils.class);

	/**
	 * Initialisation du mapper JSON
	 */
	public static void initJSONMapper() {
		ApiTestConstants.JSON_MAPPER.registerModule(new JSR310Module());
		ApiTestConstants.JSON_MAPPER.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, false);
	}

	/**
	 * Recuperation du contenu d'un fichier requete
	 * 
	 * @param resourceLoader l'objet ResourceLoader
	 * @param requestFile    le fichier de requete JSON
	 * @return le contenu du fichier requete
	 * @throws IOException une IOException
	 */
	public static String getRequestFileContent(ResourceLoader resourceLoader, String requestFile) throws IOException {
		Resource resourceFichierRequete = resourceLoader
				.getResource(requestFile + ApiTestConstants.REQUEST_FILE_EXTENSION);
		return IOUtils.toString(resourceFichierRequete.getInputStream(), StandardCharsets.UTF_8);
	}

	/**
	 * Recuperation d'une valeur JSON d'un resultat de requete HTTP
	 * 
	 * @param resultActions l'objet ResultActions
	 * @param jsonKey       la cle JSON
	 * @return la valeur JSON
	 * @throws UnsupportedEncodingException une UnsupportedEncodingException
	 * @throws ParseException               une ParseException
	 */
	public static String getRequestResultJSONValue(ResultActions resultActions, String jsonKey)
			throws UnsupportedEncodingException, ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(resultActions.andReturn().getResponse().getContentAsString());
		return json.get(jsonKey).toString();
	}

	/**
	 * Log de la response HTTP
	 * 
	 * @param resultActions l'objet result actions
	 * @throws UnsupportedEncodingException une UnsupportedEncodingException
	 */
	public static void logResponse(ResultActions resultActions) throws UnsupportedEncodingException {
		LOGGER.info("HTTP response : {}", resultActions.andReturn().getResponse().getContentAsString());
	}

	/**
	 * Checks that Spring throws a 400 error (BadRequest)
	 * 
	 * @param resultActions  the result actions
	 * @param exceptionClass the expected exception class
	 * @param errorMessage   the part of the expected error message
	 */
	public static void verifySpringBadRequest(ResultActions resultActions, Class<? extends Exception> exceptionClass,
			String errorMessage) {
		Assert.assertEquals(resultActions.andReturn().getResolvedException().getClass(), exceptionClass);
		Assert.assertTrue(resultActions.andReturn().getResolvedException().getMessage().contains(errorMessage));
	}
}
