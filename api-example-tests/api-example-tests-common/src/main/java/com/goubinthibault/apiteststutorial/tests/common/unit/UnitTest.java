package com.goubinthibault.apiteststutorial.tests.common.unit;

import org.mockito.MockitoAnnotations;

import com.goubinthibault.apiteststutorial.tests.common.GenericTest;

/**
 * Classe abstraite commune pour les tests unitaires
 * 
 * @author Thibault GOUBIN
 */
public abstract class UnitTest extends GenericTest {

	/**
	 * Initialisation de Mockito (le moteur de mocks) et des donnees
	 * 
	 * @throws Exception une exception
	 */
	@Override
	public void init() throws Exception {
		MockitoAnnotations.initMocks(this);
		initData();
	}
}
