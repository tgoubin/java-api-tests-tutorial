package com.goubinthibault.apiteststutorial.tests.common.repositories;

import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.goubinthibault.apiteststutorial.api.ApiExampleRestApplication;
import com.goubinthibault.apiteststutorial.tests.common.GenericTest;

/**
 * Classe abstraite commune pour les tests repository
 * 
 * @author Thibault GOUBIN
 */
@SpringBootTest(classes = {
		ApiExampleRestApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@AutoConfigureDataJpa
@AutoConfigureTestEntityManager
@ImportAutoConfiguration
public class RepositoryTest extends GenericTest {

	@Override
	public void init() throws Exception {
		// Nothing to do
	}
}
