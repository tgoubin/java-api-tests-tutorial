Feature: Personne

  Scenario: Je veux simuler l'integralite du cycle de vie d'une Personne (Create Update Delete)
  
    Given J'initialise des donnees de test
    
    When J'ajoute la Personne suivante :
      |nom   |prenom  |email                           |sexe |dateNaissance|
      |GOUBIN|Thibault|thibault.goubin-externe@acoss.fr|HOMME|1985-02-12   |
      
    Then Je verifie que le service a repondu 201
    
    And Je verifie que la reponse du service est la Personne suivante :
      |id|nom   |prenom  |email                           |sexe |dateNaissance|
      |6 |GOUBIN|Thibault|thibault.goubin-externe@acoss.fr|HOMME|1985-02-12   |
      
    And Je tente d'acceder a la Personne d'identifiant 6
      
    Then Je verifie que le service a repondu 200
    
    And Je verifie que la reponse du service est la Personne suivante :
      |id|nom   |prenom  |email                           |sexe |dateNaissance|
      |6 |GOUBIN|Thibault|thibault.goubin-externe@acoss.fr|HOMME|1985-02-12   |
      
    And Je verifie que les donnees contiennent 6 Personne(s)
    
    When Je modifie la Personne suivante :
      |id|nom   |prenom                   |email                           |sexe |dateNaissance|
      |6 |GOUBIN|Thibault Laurent François|thibault.goubin@inetum.world    |HOMME|1985-02-12   |
      
    Then Je verifie que le service a repondu 200
    
    And Je verifie que la reponse du service est la Personne suivante :
      |id|nom   |prenom                   |email                           |sexe |dateNaissance|
      |6 |GOUBIN|Thibault Laurent François|thibault.goubin@inetum.world    |HOMME|1985-02-12   |
      
    And Je tente d'acceder a la Personne d'identifiant 6
      
    Then Je verifie que le service a repondu 200
    
    And Je verifie que la reponse du service est la Personne suivante :
      |id|nom   |prenom                   |email                           |sexe |dateNaissance|
      |6 |GOUBIN|Thibault Laurent François|thibault.goubin@inetum.world    |HOMME|1985-02-12   |
      
    When Je supprime la Personne d'identifiant 6
      
    Then Je verifie que le service a repondu 204
    
    And Je tente d'acceder a la Personne d'identifiant 6
      
    Then Je verifie que le service a repondu 404
    
    And Je verifie que les donnees contiennent 5 Personne(s)
    