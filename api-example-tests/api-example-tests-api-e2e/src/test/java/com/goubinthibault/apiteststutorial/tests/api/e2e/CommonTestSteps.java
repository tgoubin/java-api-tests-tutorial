package com.goubinthibault.apiteststutorial.tests.api.e2e;

import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.goubinthibault.apiteststutorial.tests.api.e2e.common.CommonApiE2ETestConstants;
import com.goubinthibault.apiteststutorial.tests.common.api.e2e.ApiE2ETestSteps;

import io.cucumber.java.en.Then;

/**
 * Steps communs pour les tests API E2E
 * 
 * @author Thibault GOUBIN
 *
 */
public class CommonTestSteps extends ApiE2ETestSteps {

	/**
	 * Methode associee au step "Then Je verifie que le service a repondu {int}"
	 * 
	 * @throws Exception
	 */
	@Then("Je verifie que le service a repondu {int}")
	public void checkServiceResponse(Integer responseCode) throws Exception {
		if (HttpStatus.OK.value() == responseCode) {
			CommonApiE2ETestConstants.RESULT_ACTIONS.andExpect(MockMvcResultMatchers.status().isOk());
		} else if (HttpStatus.CREATED.value() == responseCode) {
			CommonApiE2ETestConstants.RESULT_ACTIONS.andExpect(MockMvcResultMatchers.status().isCreated());
		} else if (HttpStatus.NO_CONTENT.value() == responseCode) {
			CommonApiE2ETestConstants.RESULT_ACTIONS.andExpect(MockMvcResultMatchers.status().isNoContent());
		} else if (HttpStatus.BAD_REQUEST.value() == responseCode) {
			CommonApiE2ETestConstants.RESULT_ACTIONS.andExpect(MockMvcResultMatchers.status().isBadRequest());
		} else if (HttpStatus.NOT_FOUND.value() == responseCode) {
			CommonApiE2ETestConstants.RESULT_ACTIONS.andExpect(MockMvcResultMatchers.status().isNotFound());
		}
	}
}
