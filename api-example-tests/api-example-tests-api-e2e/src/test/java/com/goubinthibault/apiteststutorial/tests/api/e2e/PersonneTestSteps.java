package com.goubinthibault.apiteststutorial.tests.api.e2e;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.goubinthibault.apiteststutorial.api.dto.Personne;
import com.goubinthibault.apiteststutorial.api.dto.Personne.SexeEnum;
import com.goubinthibault.apiteststutorial.api.personne.PersonneFakeRepository;
import com.goubinthibault.apiteststutorial.tests.api.e2e.common.CommonApiE2ETestConstants;
import com.goubinthibault.apiteststutorial.tests.api.e2e.personne.PersonneApiE2ETestUtils;
import com.goubinthibault.apiteststutorial.tests.api.personne.PersonneApiTestConstants;
import com.goubinthibault.apiteststutorial.tests.api.personne.PersonneApiTestUtils;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestConstants;
import com.goubinthibault.apiteststutorial.tests.common.api.e2e.ApiE2ETestSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * Steps pour la feature Personne pour les tests API E2E
 * 
 * @author Thibault GOUBIN
 */
public class PersonneTestSteps extends ApiE2ETestSteps {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonneTestSteps.class);

	private static final String ID = "id";
	private static final String NOM = "nom";
	private static final String PRENOM = "prenom";
	private static final String EMAIL = "email";
	private static final String DATE_NAISSANCE = "dateNaissance";
	private static final String SEXE = "sexe";

	private static final String INIT_DATA_REQUESTS_FOLDER = PersonneApiTestConstants.POST_PERSONNE_REQUESTS_FOLDER
			+ "personneTestSteps/";

	/**
	 * Methode associee au step "Given J'initialise des donnees de test"
	 * 
	 * @throws Exception une exception
	 */
	@Given("J'initialise des donnees de test")
	public void initData() throws Exception {
		LOGGER.info("J'initialise des donnees de test");

		init();

		PersonneFakeRepository.PERSONNES = new HashSet<>();
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_1");
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_2");
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_3");
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_4");
		PersonneApiTestUtils.postPersonne(mockMvc, resourceLoader, INIT_DATA_REQUESTS_FOLDER + "initData_5");
	}

	/**
	 * Methode associee au step "Then Je tente d'acceder a la Personne d'identifiant
	 * {int}"
	 * 
	 * @param id identifiant
	 * @throws Exception une exception
	 */
	@Then("Je tente d'acceder a la Personne d'identifiant {int}")
	public void getPersonneById(Integer id) throws Exception {
		LOGGER.info("Je tente d'acceder a la Personne d'identifiant {}", id);

		// Appel de GET /personne/{id}
		CommonApiE2ETestConstants.RESULT_ACTIONS = PersonneApiTestUtils.getPersonneById(mockMvc, id);
	}

	/**
	 * Methode associee au step "When J'ajoute la Personne suivante :"
	 * 
	 * @param descriptionPersonne description de la personne
	 * @throws Exception une exception
	 */
	@When("J'ajoute la Personne suivante :")
	public void createPersonne(DataTable descriptionPersonne) throws Exception {
		LOGGER.info("J'ajoute la Personne suivante : {}", descriptionPersonne);

		// Appel de POST /personne
		CommonApiE2ETestConstants.RESULT_ACTIONS = PersonneApiE2ETestUtils.postPersonne(mockMvc,
				buildPersonneFromDescription(descriptionPersonne));
	}

	/**
	 * Methode associee au step "When Je modifie la Personne suivante :"
	 * 
	 * @param descriptionPersonne description de la personne
	 * @throws Exception une exception
	 */
	@When("Je modifie la Personne suivante :")
	public void updatePersonne(DataTable descriptionPersonne) throws Exception {
		LOGGER.info("Je modifie la Personne suivante : {}", descriptionPersonne);

		// Appel de PUT /personne/{id}
		CommonApiE2ETestConstants.RESULT_ACTIONS = PersonneApiE2ETestUtils.putPersonne(mockMvc,
				buildPersonneFromDescription(descriptionPersonne));
	}

	/**
	 * Methode associee au step "When Je supprime la Personne d'identifiant {int}"
	 * 
	 * @param id identifiant de la personne
	 * @throws Exception une exception
	 */
	@When("Je supprime la Personne d'identifiant {int}")
	public void deletePersonne(Integer id) throws Exception {
		LOGGER.info("Je supprime la Personne d'identifiant {}", id);

		// Appel de DELETE /personne/{id}
		CommonApiE2ETestConstants.RESULT_ACTIONS = PersonneApiTestUtils.deletePersonne(mockMvc, id);
	}

	/**
	 * Methode associee au step "Then Je verifie que la reponse du service est la
	 * Personne suivante :"
	 * 
	 * @param descriptionPersonne description de la personne
	 * @throws Exception une exception
	 */
	@Then("Je verifie que la reponse du service est la Personne suivante :")
	public void checkPersonneFromServiceResponse(DataTable descriptionPersonne) throws Exception {
		LOGGER.info("Je verifie que la reponse du service est la Personne suivante : {}", descriptionPersonne);

		Personne personneExpected = buildPersonneFromDescription(descriptionPersonne);
		PersonneApiTestUtils.verifyPersonneContent(CommonApiE2ETestConstants.RESULT_ACTIONS, personneExpected);
	}

	/**
	 * Methode associee au step "Then Je verifie que les donnees contiennent {int}
	 * Personne\\(s)"
	 * 
	 * @param nbPersonnes le nombre de personnes attendu
	 * @throws Exception une Exception
	 */
	@Then("Je verifie que les donnees contiennent {int} Personne\\(s)")
	public void checkDataContainsPersonne(Integer nbPersonnes) throws Exception {
		LOGGER.info("Je verifie que les donnees contiennent {} Personne\\\\(s)", nbPersonnes);

		// Appel de GET /personne et verification du contenu de la reponse
		PersonneApiTestUtils.getPersonneOK(mockMvc, null, null, null, nbPersonnes);
	}

	/**
	 * Creation d'un objet Personne a partir d'une description
	 * 
	 * @param descriptionPersonne nom, prenom, email, sexe, date de naissance
	 * @param dateNaissanceString date de naissance
	 * @return la requete JSON
	 * @throws JsonProcessingException une JsonProcessingException
	 */
	private Personne buildPersonneFromDescription(DataTable descriptionPersonne) throws JsonProcessingException {
		Map<Object, Object> descriptionPersonneMap = descriptionPersonne.asMaps(String.class, String.class).get(0);

		Personne personne = new Personne();

		if (descriptionPersonneMap.containsKey(ID)) {
			personne.setId(Integer.parseInt(descriptionPersonneMap.get(ID).toString()));
		}

		if (descriptionPersonneMap.containsKey(DATE_NAISSANCE)) {
			personne.setDateNaissance(LocalDate.parse(descriptionPersonneMap.get(DATE_NAISSANCE).toString(),
					ApiTestConstants.DATE_FORMATTER));
		}

		if (descriptionPersonneMap.containsKey(EMAIL)) {
			personne.setEmail(descriptionPersonneMap.get(EMAIL).toString().trim());
		}

		if (descriptionPersonneMap.containsKey(NOM)) {
			personne.setNom(descriptionPersonneMap.get(NOM).toString().trim());
		}

		if (descriptionPersonneMap.containsKey(PRENOM)) {
			personne.setPrenom(descriptionPersonneMap.get(PRENOM).toString().trim());
		}

		if (descriptionPersonneMap.containsKey(SEXE)) {
			personne.setSexe(SexeEnum.valueOf(descriptionPersonneMap.get(SEXE).toString().trim()));
		}

		return personne;
	}
}
