package com.goubinthibault.apiteststutorial.tests.api.e2e;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.goubinthibault.apiteststutorial.api.ApiExampleRestApplication;

import io.cucumber.spring.CucumberContextConfiguration;

/**
 * Configuration Cucumber
 * 
 * @author Thibault GOUBIN
 */
@SpringBootTest(classes = {
		ApiExampleRestApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@CucumberContextConfiguration
@AutoConfigureMockMvc
public class CucumberConfiguration {
}
