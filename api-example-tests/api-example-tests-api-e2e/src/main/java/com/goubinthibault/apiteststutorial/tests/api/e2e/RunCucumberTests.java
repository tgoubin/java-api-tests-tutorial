package com.goubinthibault.apiteststutorial.tests.api.e2e;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * Lanceur Cucumber
 * 
 * @author Thibault GOUBIN
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty" })
public class RunCucumberTests {
}
