package com.goubinthibault.apiteststutorial.tests.api.e2e.personne;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.goubinthibault.apiteststutorial.api.dto.Personne;
import com.goubinthibault.apiteststutorial.tests.api.personne.PersonneApiTestConstants;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestConstants;
import com.goubinthibault.apiteststutorial.tests.common.api.ApiTestUtils;

/**
 * Methodes utilitaires pour les tests API E2E sur Personne
 * 
 * @author Thibault GOUBIN
 */
public class PersonneApiE2ETestUtils {

	/**
	 * Execution d'un POST /personne
	 * 
	 * @param mockMvc l'objet MockMvc
	 * @param object  l'objet a traiter
	 * @return l'objet ResultActions
	 * @throws Exception une exception
	 */
	public static ResultActions postPersonne(MockMvc mockMvc, Object object) throws Exception {
		ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
				.post(PersonneApiTestConstants.PERSONNE_PATH).contentType(MediaType.APPLICATION_JSON)
				.content(ApiTestConstants.JSON_MAPPER.writeValueAsString(object)));
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}

	/**
	 * Execution d'un PUT /personne/{id}
	 * 
	 * @param mockMvc l'objet MockMvc
	 * @param object  l'objet a traiter
	 * @return l'objet ResultActions
	 * @throws Exception une exception
	 */
	public static ResultActions putPersonne(MockMvc mockMvc, Object object) throws Exception {
		ResultActions resultActions = mockMvc.perform(
				MockMvcRequestBuilders.put(PersonneApiTestConstants.PERSONNE_PATH + "/" + ((Personne) object).getId())
						.contentType(MediaType.APPLICATION_JSON)
						.content(ApiTestConstants.JSON_MAPPER.writeValueAsString(object)));
		ApiTestUtils.logResponse(resultActions);
		return resultActions;
	}
}
