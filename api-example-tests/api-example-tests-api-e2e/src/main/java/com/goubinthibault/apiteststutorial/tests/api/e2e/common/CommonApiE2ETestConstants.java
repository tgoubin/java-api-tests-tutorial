package com.goubinthibault.apiteststutorial.tests.api.e2e.common;

import org.springframework.test.web.servlet.ResultActions;

/**
 * Constantes pour les tests API E2E
 * 
 * @author Thibault GOUBIN
 */
public class CommonApiE2ETestConstants {

	public static ResultActions RESULT_ACTIONS = null;
}
