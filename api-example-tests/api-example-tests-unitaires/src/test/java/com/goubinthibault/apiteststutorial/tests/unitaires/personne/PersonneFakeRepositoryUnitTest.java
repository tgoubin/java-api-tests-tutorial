package com.goubinthibault.apiteststutorial.tests.unitaires.personne;

import java.time.LocalDate;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;
import com.goubinthibault.apiteststutorial.tests.unit.personne.PersonneUnitTest;
import com.goubinthibault.apiteststutorial.api.personne.PersonneFakeRepository;

/**
 * Tests unitaires pour la classe 'PersonneFakeRepository'
 * 
 * @author Thibault GOUBIN
 */
public class PersonneFakeRepositoryUnitTest extends PersonneUnitTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonneFakeRepositoryUnitTest.class);

	/**
	 * Test unitaire 'PersonneFakeRepository::generateIdPersonne()' - cas nominal
	 */
	@Test
	public void generateIdPersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::generateIdPersonne()' - cas nominal");

		// On genere un identifiant (donc theoriquement "nombre de personnes + 1")
		Integer id = PersonneFakeRepository.generateIdPersonne();

		// On verifie que la valeur generee est bien 6
		Assert.assertTrue(id == 6);
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::savePersonne()' - cas nominal -
	 * creation
	 */
	@Test
	public void savePersonne_OK_1() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::savePersonne()' - cas nominal - creation");

		// On ajoute une personne
		String nom = "TEST";
		String prenom = "Test";
		String email = "test@gmail.com";
		LocalDate dateNaissance = LocalDate.of(1985, 2, 12);
		SexeEnum sexe = SexeEnum.HOMME;
		PersonneFakeRepository.savePersonne(new PersonneEntity(nom, prenom, email, dateNaissance, sexe));

		// On verifie que le nombre de personnes est bien passe a 6
		Assert.assertTrue(PersonneFakeRepository.PERSONNES.size() == 6);

		// On verifie que la personne creee a bien ete sauvegardee avec les bonnes
		// valeurs
		PersonneEntity personne = PersonneFakeRepository.getPersonneById(6);
		Assert.assertEquals(personne.getNom(), nom);
		Assert.assertEquals(personne.getPrenom(), prenom);
		Assert.assertEquals(personne.getEmail(), email);
		Assert.assertEquals(personne.getDateNaissance(), dateNaissance);
		Assert.assertTrue(personne.getSexe() == sexe);
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::savePersonne()' - cas nominal -
	 * modification
	 */
	@Test
	public void savePersonne_OK_2() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::savePersonne()' - cas nominal - modification");

		// On recupere une personne pour la modifier
		Integer id = 2;
		PersonneEntity personne = PersonneFakeRepository.getPersonneById(id);
		String nom = "TEST";
		personne.setNom(nom);
		PersonneFakeRepository.savePersonne(personne);

		// On verifie que le nombre de personnes est toujours a 5
		Assert.assertTrue(PersonneFakeRepository.PERSONNES.size() == 5);

		// On verifie que la personne modifiee a bien ete sauvegardee
		personne = PersonneFakeRepository.getPersonneById(id);
		Assert.assertEquals(personne.getNom(), nom);
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::deletePersonne()' - cas nominal
	 */
	@Test
	public void deletePersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::deletePersonne()' - cas nominal");

		// Suppression d'une personne
		Integer idToDelete = 3;
		PersonneFakeRepository.deletePersonne(idToDelete);

		// On verifie que le nombre de personnes est bien passe a 4
		Assert.assertTrue(PersonneFakeRepository.PERSONNES.size() == 4);

		// On verifie que la personne modifiee a bien ete supprimee
		Assert.assertNull(PersonneFakeRepository.getPersonneById(idToDelete));
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal -
	 * scenario 1
	 */
	@Test
	public void searchPersonnes_OK_1() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal - scenario 1");

		// Recherche de personnes par nom
		String nom = "BRYANT";
		List<PersonneEntity> personnes = PersonneFakeRepository.searchPersonnes(nom, null, null);

		// On verifie que la liste contient 1 personne (dans le jeu de donnees, il n'y a
		// qu'une personne qui a ce nom)
		Assert.assertTrue(personnes.size() == 1);

		// On verifie que la personne retournee par la recherche a bien le nom "BRYANT"
		Assert.assertEquals(personnes.get(0).getNom(), nom);
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal -
	 * scenario 2
	 */
	@Test
	public void searchPersonnes_OK_2() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal - scenario 2");

		// Recherche de personnes par prenom
		String prenom = "Zinedine";
		List<PersonneEntity> personnes = PersonneFakeRepository.searchPersonnes(null, prenom, null);

		// On verifie que la liste contient 1 personne (dans le jeu de donnees, il n'y a
		// qu'une personne qui a ce prenom)
		Assert.assertTrue(personnes.size() == 1);

		// On verifie que la personne retournee par la recherche a bien le prenom
		// "Zinedine"
		Assert.assertEquals(personnes.get(0).getPrenom(), prenom);
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal -
	 * scenario 3
	 */
	@Test
	public void searchPersonnes_OK_3() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal - scenario 3");

		// Recherche d'une personne par sexe
		SexeEnum sexe = SexeEnum.FEMME;
		List<PersonneEntity> personnes = PersonneFakeRepository.searchPersonnes(null, null, sexe);

		// On verifie que la liste contient 2 personnes (dans le jeu de donnees, il y a
		// deux personnes qui ont ce sexe)
		Assert.assertTrue(personnes.size() == 2);

		// On verifie que la personne retournee par la recherche a bien le sexe "FEMME"
		Assert.assertEquals(personnes.get(0).getSexe(), sexe);
		Assert.assertEquals(personnes.get(1).getSexe(), sexe);
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal -
	 * scenario 4
	 */
	@Test
	public void searchPersonnes_OK_4() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal - scenario 4");

		// Recherche de personnes par nom, prenom et sexe
		String nom = "ROBERTS";
		String prenom = "Julia";
		SexeEnum sexe = SexeEnum.FEMME;
		List<PersonneEntity> personnes = PersonneFakeRepository.searchPersonnes(nom, prenom, sexe);

		// On verifie que la liste contient 1 personne (dans le jeu de donnees, il n'y a
		// qu'une personne qui correspond a ces informations)
		Assert.assertTrue(personnes.size() == 1);

		// On verifie que la personne retournee par la recherche a bien le nom
		// "ROBERTS", le
		// prenom "Julia" et le sexe "FEMME"
		Assert.assertEquals(personnes.get(0).getNom(), nom);
		Assert.assertEquals(personnes.get(0).getPrenom(), prenom);
		Assert.assertTrue(personnes.get(0).getSexe() == sexe);
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal -
	 * scenario 5
	 */
	@Test
	public void searchPersonnes_OK_5() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal - scenario 5");

		// Recherche de personnes par nom, prenom et sexe
		String nom = "ROBERTS";
		String prenom = "Julia";
		SexeEnum sexe = SexeEnum.HOMME;
		List<PersonneEntity> personnes = PersonneFakeRepository.searchPersonnes(nom, prenom, sexe);

		// On verifie que la liste contient 0 personne (dans le jeu de donnees, il n'y a
		// aucune personne qui correspond a ces informations)
		Assert.assertTrue(personnes.isEmpty());
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal -
	 * scenario 6
	 */
	@Test
	public void searchPersonnes_OK_6() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::searchPersonnes()' - cas nominal - scenario 6");

		// Recherche de personnes avec aucun critere
		List<PersonneEntity> personnes = PersonneFakeRepository.searchPersonnes(null, null, null);

		// On verifie que la liste contient 5 personnes (l'ensemble du jeu de donnees)
		Assert.assertTrue(personnes.size() == PersonneFakeRepository.PERSONNES.size());
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::getPersonneById()' - cas nominal -
	 * scenario 1
	 */
	@Test
	public void getPersonneById_OK_1() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::getPersonneById()' - cas nominal - scenario 1");

		// Tentative de recuperation d'une personne qui existe
		// Verification qu'une personne est recuperee
		Assert.assertNotNull(PersonneFakeRepository.getPersonneById(2));
	}

	/**
	 * Test unitaire 'PersonneFakeRepository::getPersonneById()' - cas nominal -
	 * scenario 2
	 */
	@Test
	public void getPersonneById_OK_2() {
		LOGGER.info("Test unitaire 'PersonneFakeRepository::getPersonneById()' - cas nominal - scenario 2");

		// Tentative de recuperation d'une personne qui n'existe pas
		// Verification qu'aucune personne n'est recuperee
		Assert.assertNull(PersonneFakeRepository.getPersonneById(10));
	}
}
