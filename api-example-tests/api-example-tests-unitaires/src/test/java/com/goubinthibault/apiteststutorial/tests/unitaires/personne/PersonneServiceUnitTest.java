package com.goubinthibault.apiteststutorial.tests.unitaires.personne;

import java.time.LocalDate;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.goubinthibault.apiteststutorial.api.exception.BadRequestException;
import com.goubinthibault.apiteststutorial.api.exception.ResourceNotFoundException;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;
import com.goubinthibault.apiteststutorial.tests.unit.personne.PersonneUnitTest;
import com.goubinthibault.apiteststutorial.api.personne.PersonneFakeRepository;
import com.goubinthibault.apiteststutorial.api.personne.PersonneService;

/**
 * Tests unitaires pour la classe 'PersonneService'
 * 
 * @author Thibault GOUBIN
 */
public class PersonneServiceUnitTest extends PersonneUnitTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonneServiceUnitTest.class);

	/**
	 * Service a tester
	 */
	// Quand la classe a tester est un composant manage par Spring, il faut utiliser
	// l'annotation @InjectMocks
	@InjectMocks
	private PersonneService personneService;

	/**
	 * Test unitaire 'PersonneService::searchPersonnes()' - cas nominal
	 */
	@Test
	public void searchPersonnes_OK() {
		LOGGER.info("Test unitaire 'PersonneService::searchPersonnes()' - cas nominal");

		// Recherche d'une personne par nom, prenom et sexe
		String nom = "HENRY";
		String prenom = "Amandine";
		SexeEnum sexe = SexeEnum.FEMME;
		List<PersonneEntity> personnes = personneService.searchPersonnes(nom, prenom, sexe);

		// On verifie que la liste contient 0 personne (dans le jeu de donnees, il n'y a
		// aucune personne qui correspond a ces informations)
		Assert.assertEquals(personnes.get(0).getNom(), nom);
		Assert.assertEquals(personnes.get(0).getPrenom(), prenom);
		Assert.assertTrue(personnes.get(0).getSexe() == sexe);
	}

	/**
	 * Test unitaire 'PersonneService::getPersonneById()' - cas nominal
	 */
	@Test
	public void getPersonneById_OK() {
		LOGGER.info("Test unitaire 'PersonneService::getPersonneById()' - cas nominal");

		// Tentative de recuperation d'une personne qui existe
		// Verification qu'une personne est recuperee
		Assert.assertNotNull(personneService.getPersonneById(2));
	}

	/**
	 * Test unitaire 'PersonneService::getPersonneById()' - cas d'erreur
	 */
	@Test
	public void getPersonneById_KO() {
		LOGGER.info("Test unitaire 'PersonneService::getPersonneById()' - cas d'erreur");

		// Verification que pour un 'getPersonneById()' d'un identifiant inexistant, une
		// exception ResourceNotFoundException est bien levee
		Integer id = 10;
		Assertions.assertThatThrownBy(() -> {
			personneService.getPersonneById(id);
		}).isInstanceOf(ResourceNotFoundException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Il n'y a aucune Personne avec l'identifiant '" + id + "'");
	}

	/**
	 * Test unitaire 'PersonneService::createPersonne()' - cas nominal
	 */
	@Test
	public void createPersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneService::createPersonne()' - cas nominal");

		// On ajoute une personne
		String nom = "TEST";
		String prenom = "Test";
		String email = "test@yahoo.fr";
		LocalDate dateNaissance = LocalDate.of(1986, 8, 5);
		SexeEnum sexe = SexeEnum.FEMME;
		personneService.createPersonne(new PersonneEntity(nom, prenom, email, dateNaissance, sexe));

		// On verifie que le nombre de personnes est bien passe a 6
		Assert.assertTrue(PersonneFakeRepository.PERSONNES.size() == 6);

		// On verifie que la personne creee a bien ete sauvegardee avec les bonnes
		// valeurs
		PersonneEntity personne = PersonneFakeRepository.getPersonneById(6);
		Assert.assertEquals(personne.getNom(), nom);
		Assert.assertEquals(personne.getPrenom(), prenom);
		Assert.assertEquals(personne.getEmail(), email);
		Assert.assertEquals(personne.getDateNaissance(), dateNaissance);
		Assert.assertTrue(personne.getSexe() == sexe);
	}

	/**
	 * Test unitaire 'PersonneService::createPersonne()' - cas d'erreur - champs
	 * obligatoires manquants
	 */
	@Test
	public void createPersonne_KO_ChampsObligatoiresManquants() {
		LOGGER.info("Test unitaire 'PersonneService::createPersonne()' - cas d'erreur - champs obligatoires manquants");

		// Verification que pour un 'createPersonne()', une
		// exception BadRequestException est bien levee quand les champs obligatoires ne
		// sont pas renseignes
		Assertions.assertThatThrownBy(() -> {
			personneService.createPersonne(
					new PersonneEntity(null, "Test", "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'nom' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			personneService.createPersonne(
					new PersonneEntity("", "Test", "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'nom' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			personneService.createPersonne(
					new PersonneEntity("TEST", null, "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'prenom' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			personneService.createPersonne(
					new PersonneEntity("TEST", " ", "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'prenom' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			personneService.createPersonne(new PersonneEntity("TEST", "Test", "test@gmail.com", null, SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'dateNaissance' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			personneService.createPersonne(
					new PersonneEntity("TEST", "Test", "test@gmail.com", LocalDate.of(1986, 8, 5), null));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'sexe' est obligatoire");
	}

	/**
	 * Test unitaire 'PersonneService::createPersonne()' - cas d'erreur - email
	 * invalide
	 */
	@Test
	public void createPersonne_KO_EmailInvalide() {
		LOGGER.info("Test unitaire 'PersonneService::createPersonne()' - cas d'erreur - email invalide");

		// Verification que pour un 'createPersonne()', une
		// exception BadRequestException est bien levee quand l'email est invalide
		String email = "test@yahoo";
		Assertions.assertThatThrownBy(() -> {
			personneService.createPersonne(
					new PersonneEntity("TEST", "Test", email, LocalDate.of(1986, 8, 5), SexeEnum.FEMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"L'email '" + email + "' n'est pas valide");
	}

	/**
	 * Test unitaire 'PersonneService::updatePersonne()' - cas nominal
	 */
	@Test
	public void updatePersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneService::updatePersonne()' - cas nominal");

		// On recupere une personne pour la modifier
		Integer id = 2;
		PersonneEntity personne = PersonneFakeRepository.getPersonneById(id);
		String prenom = "Test";
		personne.setPrenom(prenom);
		personneService.updatePersonne(id, personne);

		// On verifie que le nombre de personnes est toujours a 5
		Assert.assertTrue(PersonneFakeRepository.PERSONNES.size() == 5);

		// On verifie que la personne modifiee a bien ete sauvegardee
		personne = PersonneFakeRepository.getPersonneById(id);
		Assert.assertEquals(personne.getPrenom(), prenom);
	}

	/**
	 * Test unitaire 'PersonneService::updatePersonne()' - cas d'erreur -
	 * ResourceNotFoundException
	 */
	@Test
	public void updatePersonne_KO_ResourceNotFoundException() {
		LOGGER.info("Test unitaire 'PersonneService::updatePersonne()' - cas d'erreur - ResourceNotFoundException");

		// Verification que pour un 'updatePersonne()' d'un identifiant inexistant, une
		// exception ResourceNotFoundException est bien levee
		Integer id = 10;
		Assertions.assertThatThrownBy(() -> {
			personneService.updatePersonne(id, PersonneFakeRepository.getPersonneById(id));
		}).isInstanceOf(ResourceNotFoundException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Il n'y a aucune Personne avec l'identifiant '" + id + "'");
	}

	/**
	 * Test unitaire 'PersonneService::updatePersonne()' - cas d'erreur -
	 * BadRequestException - les identifiants ne correspondent pas
	 */
	@Test
	public void updatePersonne_KO_BadRequestException_Identifiants() {
		LOGGER.info(
				"Test unitaire 'PersonneService::updatePersonne()' - cas d'erreur - BadRequestException - les identifiants ne correspondent pas");

		// Verification que pour un 'updatePersonne()', une
		// exception BadRequestException est bien levee quand l'identifiant passe en
		// parametre est different de l'identifiant contenu
		// par l'entite
		Integer id1 = 2;
		Integer id2 = 3;
		Assertions.assertThatThrownBy(() -> {
			personneService.updatePersonne(id1, PersonneFakeRepository.getPersonneById(id2));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Les deux identifiants ne correspondent pas");
	}

	/**
	 * Test unitaire 'PersonneService::updatePersonne()' - cas d'erreur - champs
	 * obligatoires manquants
	 */
	@Test
	public void updatePersonne_KO_ChampsObligatoiresManquants() {
		LOGGER.info("Test unitaire 'PersonneService::updatePersonne()' - cas d'erreur - champs obligatoires manquants");

		// Verification que pour un 'createPersonne()', une
		// exception BadRequestException est bien levee quand les champs obligatoires ne
		// sont pas renseignes
		Assertions.assertThatThrownBy(() -> {
			Integer id = 1;
			personneService.updatePersonne(id,
					new PersonneEntity(id, null, "Test", "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'nom' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			Integer id = 1;
			personneService.updatePersonne(id,
					new PersonneEntity(id, "", "Test", "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'nom' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			Integer id = 2;
			personneService.updatePersonne(id,
					new PersonneEntity(id, "TEST", null, "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'prenom' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			Integer id = 2;
			personneService.updatePersonne(id,
					new PersonneEntity(id, "TEST", " ", "test@gmail.com", LocalDate.of(1985, 2, 12), SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'prenom' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			Integer id = 3;
			personneService.updatePersonne(id,
					new PersonneEntity(id, "TEST", "Test", "test@gmail.com", null, SexeEnum.HOMME));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'dateNaissance' est obligatoire");
		Assertions.assertThatThrownBy(() -> {
			Integer id = 4;
			personneService.updatePersonne(id,
					new PersonneEntity(id, "TEST", "Test", "test@gmail.com", LocalDate.of(1986, 8, 5), null));
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Le champ 'sexe' est obligatoire");
	}

	/**
	 * Test unitaire 'PersonneService::updatePersonne()' - cas d'erreur -
	 * BadRequestException - email invalide
	 */
	@Test
	public void updatePersonne_KO_BadRequestException_Email() {
		LOGGER.info(
				"Test unitaire 'PersonneService::updatePersonne()' - cas d'erreur - BadRequestException - email invalide");

		// Verification que pour un 'createPersonne()', une
		// exception BadRequestException est bien levee quand l'email est invalide
		Integer id = 2;
		String email = "mauvais.email@acoss";
		PersonneEntity personne = PersonneFakeRepository.getPersonneById(id);
		personne.setEmail(email);
		Assertions.assertThatThrownBy(() -> {
			personneService.updatePersonne(id, personne);
		}).isInstanceOf(BadRequestException.class).hasFieldOrPropertyWithValue("messageErreur",
				"L'email '" + email + "' n'est pas valide");
	}

	/**
	 * Test unitaire 'PersonneService::deletePersonne()' - cas nominal
	 */
	@Test
	public void deletePersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneService::deletePersonne()' - cas nominal");

		// Suppression d'une personne
		Integer idToDelete = 3;
		personneService.deletePersonne(idToDelete);

		// On verifie que le nombre de personnes est bien passe a 4
		Assert.assertTrue(PersonneFakeRepository.PERSONNES.size() == 4);

		// On verifie que la personne modifiee a bien ete supprimee
		Assert.assertNull(PersonneFakeRepository.getPersonneById(idToDelete));
	}

	/**
	 * Test unitaire 'PersonneService::deletePersonne()' - cas d'erreur
	 */
	@Test
	public void deletePersonne_KO() {
		LOGGER.info("Test unitaire 'PersonneService::deletePersonne()' - cas d'erreur");

		// Verification que pour un 'deletePersonne()' d'un identifiant inexistant, une
		// exception ResourceNotFoundException est bien levee
		Integer id = 10;
		Assertions.assertThatThrownBy(() -> {
			personneService.deletePersonne(id);
		}).isInstanceOf(ResourceNotFoundException.class).hasFieldOrPropertyWithValue("messageErreur",
				"Il n'y a aucune Personne avec l'identifiant '" + id + "'");
	}
}
