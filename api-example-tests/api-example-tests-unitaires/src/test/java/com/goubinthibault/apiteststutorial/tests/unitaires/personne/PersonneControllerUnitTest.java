package com.goubinthibault.apiteststutorial.tests.unitaires.personne;

import java.time.LocalDate;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.goubinthibault.apiteststutorial.api.dto.Personne;
import com.goubinthibault.apiteststutorial.api.dto.Sexe;
import com.goubinthibault.apiteststutorial.api.exception.BadRequestException;
import com.goubinthibault.apiteststutorial.api.exception.ResourceNotFoundException;
import com.goubinthibault.apiteststutorial.api.personne.PersonneController;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;
import com.goubinthibault.apiteststutorial.tests.unit.personne.PersonneUnitTest;
import com.goubinthibault.apiteststutorial.api.personne.PersonneFakeRepository;
import com.goubinthibault.apiteststutorial.api.personne.PersonneMapper;
import com.goubinthibault.apiteststutorial.api.personne.PersonneService;

/**
 * Tests unitaires pour la classe 'PersonneController'
 * 
 * @author Thibault GOUBIN
 */
public class PersonneControllerUnitTest extends PersonneUnitTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonneServiceUnitTest.class);

	/**
	 * Controller a tester
	 */
	// Quand la classe a tester est un composant manage par Spring, il faut utiliser
	// l'annotation @InjectMocks
	@InjectMocks
	private PersonneController personneController;

	/**
	 * Service 'PersonneService'
	 */
	// Quand la classe a tester utilise elle-meme un (ou plusieurs) composant(s)
	// manage(s) par Spring, ces derniers doivent
	// etre "mockes" (avec l'annotation @Mock) pour en diriger le comportement
	@Mock
	private PersonneService personneService;

	/**
	 * Test unitaire 'PersonneController::recherchePersonnes()' - cas nominal
	 */
	@Test
	public void recherchePersonnes_OK() {
		LOGGER.info("Test unitaire 'PersonneController::recherchePersonnes()' - cas nominal");

		// On simule le comportement de 'PersonneService::searchPersonnes()'
		String nom = null;
		String prenom = null;
		SexeEnum sexe = SexeEnum.FEMME;
		List<PersonneEntity> personneEntities = PersonneFakeRepository.searchPersonnes(nom, prenom, sexe);
		Mockito.when(
				personneService.searchPersonnes(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
				.thenReturn(personneEntities);

		// Recherche de personnes
		ResponseEntity<List<Personne>> response = personneController.recherchePersonnes(nom, prenom,
				Sexe.fromValue(sexe.name()));
		// Verification du code de la reponse HTTP
		Assert.assertTrue(response.getStatusCode() == HttpStatus.OK);
		// Verification des valeurs
		for (int i = 0; i < response.getBody().size(); i++) {
			verifyResponseContent(response.getBody().get(i).getId(), response.getBody().get(i),
					personneEntities.get(i));
		}
	}

	/**
	 * Test unitaire 'PersonneController::recuperationPersonne()' - cas nominal
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void recuperationPersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneController::recuperationPersonne()' - cas nominal");

		// On simule le comportement de 'PersonneService::getPersonneById()'
		Integer id = 4;
		PersonneEntity personneEntity = PersonneFakeRepository.getPersonneById(id);
		Mockito.when(personneService.getPersonneById(ArgumentMatchers.any())).thenReturn(personneEntity);

		// Recuperation d'une personne par son identifiant
		ResponseEntity<Personne> response = personneController.recuperationPersonne(id);
		// Verification du code de la reponse HTTP
		Assert.assertTrue(response.getStatusCode() == HttpStatus.OK);
		// Verification des valeurs
		verifyResponseContent(id, response.getBody(), personneEntity);

		// Verification des appels a l'interieur de la methode
		// 'PersonneController::recuperationPersonne()'
		Mockito.verify(personneService, Mockito.times(1)).getPersonneById(ArgumentMatchers.any());
	}

	/**
	 * Test unitaire 'PersonneController::recuperationPersonne()' - cas d'erreur
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void recuperationPersonne_KO() {
		LOGGER.info("Test unitaire 'PersonneController::recuperationPersonne()' - cas d'erreur");

		// On simule le comportement de 'PersonneService::getPersonneById()'
		Mockito.when(personneService.getPersonneById(ArgumentMatchers.any()))
				.thenThrow(ResourceNotFoundException.class);

		// Tentative de recuperation d'une personne par son identifiant
		ResponseEntity<String> response = personneController.recuperationPersonne(10);

		// Verification du code de l'erreur
		Assert.assertTrue(response.getStatusCode() == HttpStatus.NOT_FOUND);
	}

	/**
	 * Test unitaire 'PersonneController::creationPersonne()' - cas nominal
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void creationPersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneController::creationPersonne()' - cas nominal");

		// On simule le comportement de 'PersonneService::createPersonne()'
		String nom = "TEST";
		String prenom = "Test";
		String email = "test@yahoo.fr";
		LocalDate dateNaissance = LocalDate.of(1986, 8, 5);
		SexeEnum sexe = SexeEnum.FEMME;
		PersonneEntity personneEntity = new PersonneEntity(nom, prenom, email, dateNaissance, sexe);
		Mockito.when(personneService.createPersonne(ArgumentMatchers.any())).thenReturn(personneEntity);
		Personne personne = PersonneMapper.entityToDto(personneEntity);

		// Creation d'une personne
		ResponseEntity<Personne> response = personneController.creationPersonne(personne);
		// Verification du code de la reponse HTTP
		Assert.assertTrue(response.getStatusCode() == HttpStatus.CREATED);
		// Verification des valeurs
		verifyResponseContent(6, response.getBody(), personneEntity);

		// Verification des appels a l'interieur de la methode
		// 'PersonneController::creationPersonne()'
		Mockito.verify(personneService, Mockito.times(1)).createPersonne(ArgumentMatchers.any());
	}

	/**
	 * Test unitaire 'PersonneController::creationPersonne()' - cas d'erreur
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void creationPersonne_KO() {
		LOGGER.info("Test unitaire 'PersonneController::creationPersonne()' - cas d'erreur");

		// On simule le comportement de 'PersonneService::createPersonne()'
		Mockito.when(personneService.createPersonne(ArgumentMatchers.any())).thenThrow(BadRequestException.class);

		// Tentative de recuperation d'une personne par son identifiant
		ResponseEntity<String> response = personneController.creationPersonne(new Personne());

		// Verification du code de l'erreur
		Assert.assertTrue(response.getStatusCode() == HttpStatus.BAD_REQUEST);
	}

	/**
	 * Test unitaire 'PersonneController::modificationPersonne()' - cas nominal
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void modificationPersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneController::modificationPersonne()' - cas nominal");

		// On simule le comportement de 'PersonneService::updatePersonne()'
		Integer id = 1;
		PersonneEntity personneEntity = PersonneFakeRepository.getPersonneById(id);
		personneEntity.setEmail("jordan.michael@hotmail.com");
		Mockito.when(personneService.updatePersonne(ArgumentMatchers.any(), ArgumentMatchers.any()))
				.thenReturn(personneEntity);
		Personne personne = PersonneMapper.entityToDto(personneEntity);

		// Modification d'une personne
		ResponseEntity<Personne> response = personneController.modificationPersonne(id, personne);
		// Verification du code de la reponse HTTP
		Assert.assertTrue(response.getStatusCode() == HttpStatus.OK);
		// Verification des valeurs
		verifyResponseContent(1, response.getBody(), personneEntity);

		// Verification des appels a l'interieur de la methode
		// 'PersonneController::modificationPersonne()'
		Mockito.verify(personneService, Mockito.times(1)).updatePersonne(ArgumentMatchers.any(),
				ArgumentMatchers.any());
	}

	/**
	 * Test unitaire 'PersonneController::modificationPersonne()' - cas d'erreur -
	 * ResourceNotFoundException
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void modificationPersonne_KO_404() {
		LOGGER.info(
				"Test unitaire 'PersonneController::modificationPersonne()' - cas d'erreur - ResourceNotFoundException");

		// On simule le comportement de 'PersonneService::createPersonne()'
		Mockito.when(personneService.updatePersonne(ArgumentMatchers.any(), ArgumentMatchers.any()))
				.thenThrow(ResourceNotFoundException.class);

		// Tentative de recuperation d'une personne par son identifiant
		ResponseEntity<String> response = personneController.modificationPersonne(10, new Personne());

		// Verification du code de l'erreur
		Assert.assertTrue(response.getStatusCode() == HttpStatus.NOT_FOUND);
	}

	/**
	 * Test unitaire 'PersonneController::modificationPersonne()' - cas d'erreur -
	 * BadRequestException
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void modificationPersonne_KO_400() {
		LOGGER.info("Test unitaire 'PersonneController::modificationPersonne()' - cas d'erreur - BadRequestException");

		// On simule le comportement de 'PersonneService::createPersonne()'
		Mockito.when(personneService.updatePersonne(ArgumentMatchers.any(), ArgumentMatchers.any()))
				.thenThrow(BadRequestException.class);

		// Tentative de recuperation d'une personne par son identifiant
		ResponseEntity<String> response = personneController.modificationPersonne(1, new Personne());

		// Verification du code de l'erreur
		Assert.assertTrue(response.getStatusCode() == HttpStatus.BAD_REQUEST);
	}

	/**
	 * Test unitaire 'PersonneController::suppressionPersonne()' - cas nominal
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void suppressionPersonne_OK() {
		LOGGER.info("Test unitaire 'PersonneController::suppressionPersonne()' - cas nominal");

		// On simule le comportement de 'PersonneService::getPersonneById()'
		Mockito.doNothing().when(personneService).deletePersonne(ArgumentMatchers.any());

		// Suppression d'une personne
		ResponseEntity<Void> response = personneController.suppressionPersonne(5);
		// Verification du code de la reponse HTTP
		Assert.assertTrue(response.getStatusCode() == HttpStatus.NO_CONTENT);

		// Verification des appels a l'interieur de la methode
		// 'PersonneController::suppressionPersonne()'
		Mockito.verify(personneService, Mockito.times(1)).deletePersonne(ArgumentMatchers.any());
	}

	/**
	 * Test unitaire 'PersonneController::suppressionPersonne()' - cas d'erreur
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void suppressionPersonne_KO() {
		LOGGER.info("Test unitaire 'PersonneController::suppressionPersonne()' - cas d'erreur");

		// On simule le comportement de 'PersonneService::createPersonne()'
		Mockito.doThrow(ResourceNotFoundException.class).when(personneService).deletePersonne(ArgumentMatchers.any());

		// Tentative de recuperation d'une personne par son identifiant
		ResponseEntity<String> response = personneController.suppressionPersonne(10);

		// Verification du code de l'erreur
		Assert.assertTrue(response.getStatusCode() == HttpStatus.NOT_FOUND);
	}

	/**
	 * Verification du contenu de la reponse
	 * 
	 * @param id             l'identifiant attendu
	 * @param personne       le dto personne
	 * @param personneEntity l'entite personne attendue
	 */
	private void verifyResponseContent(Integer id, Personne personne, PersonneEntity personneEntity) {
		Assert.assertTrue(personne.getId() == id);
		Assert.assertTrue(personne.getId() == personneEntity.getId());
		Assert.assertEquals(personne.getNom(), personneEntity.getNom());
		Assert.assertEquals(personne.getPrenom(), personneEntity.getPrenom());
		Assert.assertEquals(personne.getEmail(), personneEntity.getEmail());
		Assert.assertEquals(personne.getDateNaissance(), personneEntity.getDateNaissance());
		Assert.assertEquals(personne.getSexe().name(), personneEntity.getSexe().name());
	}
}
