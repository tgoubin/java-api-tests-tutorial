package com.goubinthibault.apiteststutorial.tests.unit.personne;

import java.time.LocalDate;
import java.util.HashSet;

import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;
import com.goubinthibault.apiteststutorial.tests.common.unit.UnitTest;
import com.goubinthibault.apiteststutorial.api.personne.PersonneFakeRepository;

/**
 * Classe abstraite commune pour les tests unitaires sur Personne
 * 
 * @author Thibault GOUBIN
 */
public abstract class PersonneUnitTest extends UnitTest {

	@Override
	public void initData() {
		PersonneFakeRepository.PERSONNES = new HashSet<>();
		PersonneFakeRepository.PERSONNES.add(new PersonneEntity("JORDAN", "Michael", "michael.jordan@gmail.com",
				LocalDate.of(1963, 2, 2), SexeEnum.HOMME));
		PersonneFakeRepository.PERSONNES.add(
				new PersonneEntity("BRYANT", "Kobe", "kobe824@orange.fr", LocalDate.of(1978, 3, 4), SexeEnum.HOMME));
		PersonneFakeRepository.PERSONNES.add(new PersonneEntity("ROBERTS", "Julia", "roberts.julia@gmail.com",
				LocalDate.of(1965, 4, 6), SexeEnum.FEMME));
		PersonneFakeRepository.PERSONNES.add(
				new PersonneEntity("ZIDANE", "Zinedine", "zizou@hotmail.fr", LocalDate.of(1972, 5, 8), SexeEnum.HOMME));
		PersonneFakeRepository.PERSONNES.add(new PersonneEntity("HENRY", "Amandine", "amandine.henry@yahoo.fr",
				LocalDate.of(1985, 6, 10), SexeEnum.FEMME));
	}
}
