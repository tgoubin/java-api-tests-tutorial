package com.goubinthibault.apiteststutorial.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.goubinthibault.apiteststutorial")
public class ApiExampleRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiExampleRestApplication.class);
	}
}
