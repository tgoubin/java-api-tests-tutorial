package com.goubinthibault.apiteststutorial.api.personne;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.goubinthibault.apiteststutorial.api.PersonneApi;
import com.goubinthibault.apiteststutorial.api.dto.Personne;
import com.goubinthibault.apiteststutorial.api.dto.Sexe;
import com.goubinthibault.apiteststutorial.api.exception.HTTPException;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;

import io.swagger.annotations.ApiParam;

@Controller
public class PersonneController implements PersonneApi {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonneController.class);

	private PersonneService personneService;

	public PersonneController(PersonneService personneService) {
		this.personneService = personneService;
	}

	@Override
	public ResponseEntity<List<Personne>> recherchePersonnes(
			@ApiParam(value = "Recherche par nom") @Valid @RequestParam(value = "nom", required = false) String nom,
			@ApiParam(value = "Recherche par prenom") @Valid @RequestParam(value = "prenom", required = false) String prenom,
			@ApiParam(value = "Recherche par sexe", allowableValues = "HOMME, FEMME") @Valid @RequestParam(value = "sexe", required = false) Sexe sexe) {

		LOGGER.info("GET /personne");

		return new ResponseEntity<List<Personne>>(PersonneMapper.entitiesToDtos(
				personneService.searchPersonnes(nom, prenom, (sexe != null) ? SexeEnum.valueOf(sexe.name()) : null)),
				HttpStatus.OK);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity recuperationPersonne(
			@ApiParam(value = "Identifiant de la personne", required = true) @PathVariable("id") Integer id) {

		LOGGER.info("GET /personne/{}", id);

		try {
			return new ResponseEntity<Personne>(PersonneMapper.entityToDto(personneService.getPersonneById(id)),
					HttpStatus.OK);
		} catch (HTTPException e) {
			return new ResponseEntity<String>(e.getMessageErreur(), HttpStatus.valueOf(e.getHTTPStatusCode()));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity creationPersonne(
			@ApiParam(value = "Donnees de la personne", required = true) @Valid @RequestBody Personne personne) {

		LOGGER.info("POST /personne");

		try {
			return new ResponseEntity<Personne>(
					PersonneMapper.entityToDto(personneService.createPersonne(PersonneMapper.dtoToEntity(personne))),
					HttpStatus.CREATED);
		} catch (HTTPException e) {
			return new ResponseEntity<String>(e.getMessageErreur(), HttpStatus.valueOf(e.getHTTPStatusCode()));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity modificationPersonne(
			@ApiParam(value = "Identifiant de la personne", required = true) @PathVariable("id") Integer id,
			@ApiParam(value = "Donnees de la personne", required = true) @Valid @RequestBody Personne personne) {

		LOGGER.info("PUT /personne/{}", id);

		try {
			return new ResponseEntity<Personne>(PersonneMapper.entityToDto(
					personneService.updatePersonne(id, PersonneMapper.dtoToEntity(personne))), HttpStatus.OK);
		} catch (HTTPException e) {
			return new ResponseEntity<String>(e.getMessageErreur(), HttpStatus.valueOf(e.getHTTPStatusCode()));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity suppressionPersonne(
			@ApiParam(value = "Identifiant de la personne", required = true) @PathVariable("id") Integer id) {

		LOGGER.info("DELETE /personne/{}", id);

		try {
			personneService.deletePersonne(id);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (HTTPException e) {
			return new ResponseEntity<String>(e.getMessageErreur(), HttpStatus.valueOf(e.getHTTPStatusCode()));
		}
	}
}
