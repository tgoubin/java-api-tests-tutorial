package com.goubinthibault.apiteststutorial.api.exception;

public class ResourceNotFoundException extends HTTPException {

	private static final long serialVersionUID = 6786701504730842901L;

	public ResourceNotFoundException(String messageErreur) {
		super(messageErreur);
	}

	@Override
	public int getHTTPStatusCode() {
		return 404;
	}
}
