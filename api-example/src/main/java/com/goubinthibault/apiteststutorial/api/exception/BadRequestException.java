package com.goubinthibault.apiteststutorial.api.exception;

public class BadRequestException extends HTTPException {

	private static final long serialVersionUID = 1125613643190935319L;

	public BadRequestException(String messageErreur) {
		super(messageErreur);
	}

	@Override
	public int getHTTPStatusCode() {
		return 400;
	}
}
