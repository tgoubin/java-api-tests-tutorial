package com.goubinthibault.apiteststutorial.api.exception;

public abstract class HTTPException extends RuntimeException {

	private static final long serialVersionUID = 7356663254354436060L;

	private String messageErreur;

	public HTTPException(String messageErreur) {
		super();
		this.messageErreur = messageErreur;
	}

	public String getMessageErreur() {
		return messageErreur;
	}

	public abstract int getHTTPStatusCode();
}
