package com.goubinthibault.apiteststutorial.api.personne;

import java.util.ArrayList;
import java.util.List;

import com.goubinthibault.apiteststutorial.api.dto.Personne;

public class PersonneMapper {

	public static PersonneEntity dtoToEntity(Personne dto) {
		PersonneEntity entity = new PersonneEntity();
		entity.setDateNaissance(dto.getDateNaissance());
		entity.setEmail(dto.getEmail());
		entity.setId(dto.getId());
		entity.setNom(dto.getNom());
		entity.setPrenom(dto.getPrenom());

		if (dto.getSexe() != null) {
			entity.setSexe(com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum
					.valueOf(dto.getSexe().name()));
		}

		return entity;
	}

	public static Personne entityToDto(PersonneEntity entity) {
		Personne dto = new Personne();
		dto.setDateNaissance(entity.getDateNaissance());
		dto.setEmail(entity.getEmail());
		dto.setId(entity.getId());
		dto.setNom(entity.getNom());
		dto.setPrenom(entity.getPrenom());

		if (entity.getSexe() != null) {
			dto.setSexe(com.goubinthibault.apiteststutorial.api.dto.Personne.SexeEnum.valueOf(entity.getSexe().name()));
		}

		return dto;
	}

	public static List<Personne> entitiesToDtos(List<PersonneEntity> entities) {
		List<Personne> dtos = new ArrayList<>();

		for (PersonneEntity entity : entities) {
			dtos.add(entityToDto(entity));
		}

		return dtos;
	}
}
