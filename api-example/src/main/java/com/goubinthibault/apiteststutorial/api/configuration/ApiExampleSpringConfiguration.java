package com.goubinthibault.apiteststutorial.api.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.goubinthibault.apiteststutorial.api")
public class ApiExampleSpringConfiguration {
}
