package com.goubinthibault.apiteststutorial.api.personne;

import java.text.MessageFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.goubinthibault.apiteststutorial.api.exception.BadRequestException;
import com.goubinthibault.apiteststutorial.api.exception.ResourceNotFoundException;
import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;

@Service
public class PersonneService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonneService.class);

	public List<PersonneEntity> searchPersonnes(String nom, String prenom, SexeEnum sexe) {
		LOGGER.info("Recherche de personnes : nom='{}', prenom='{}', sexe='{}'", nom, prenom, sexe);
		return PersonneFakeRepository.searchPersonnes(nom, prenom, sexe);
	}

	public PersonneEntity getPersonneById(Integer id) {
		LOGGER.info("Recuperation de la personne - id='{}'", id);

		PersonneEntity personne = PersonneFakeRepository.getPersonneById(id);

		if (personne == null) {
			String messageErreur = "Il n'y a aucune Personne avec l'identifiant '" + id + "'";
			LOGGER.warn(messageErreur);
			throw new ResourceNotFoundException(messageErreur);
		}

		return personne;
	}

	public PersonneEntity createPersonne(PersonneEntity personne) {
		LOGGER.info("Creation de la personne '{}'", personne);

		checkMandatoryValues(personne);
		checkEmail(personne.getEmail());

		personne.setId(PersonneFakeRepository.generateIdPersonne());
		PersonneFakeRepository.savePersonne(personne);

		return personne;
	}

	public PersonneEntity updatePersonne(Integer id, PersonneEntity personne) {
		LOGGER.info("Modification de la personne '{}'", personne);

		getPersonneById(id);

		if (id == null || id != personne.getId()) {
			String messageErreur = "Les deux identifiants ne correspondent pas";
			LOGGER.warn(messageErreur);
			throw new BadRequestException(messageErreur);
		}

		checkMandatoryValues(personne);
		checkEmail(personne.getEmail());

		PersonneFakeRepository.savePersonne(personne);

		return personne;
	}

	private void checkMandatoryValues(PersonneEntity personne) {
		String messageErreurPattern = "Le champ ''{0}'' est obligatoire";

		if (personne.getNom() == null || "".equals(personne.getNom().trim())) {
			String messageErreur = MessageFormat.format(messageErreurPattern, "nom");
			LOGGER.warn(messageErreur);
			throw new BadRequestException(messageErreur);
		}

		if (personne.getPrenom() == null || "".equals(personne.getPrenom().trim())) {
			String messageErreur = MessageFormat.format(messageErreurPattern, "prenom");
			LOGGER.warn(messageErreur);
			throw new BadRequestException(messageErreur);
		}

		if (personne.getDateNaissance() == null) {
			String messageErreur = MessageFormat.format(messageErreurPattern, "dateNaissance");
			LOGGER.warn(messageErreur);
			throw new BadRequestException(messageErreur);
		}

		if (personne.getSexe() == null) {
			String messageErreur = MessageFormat.format(messageErreurPattern, "sexe");
			LOGGER.warn(messageErreur);
			throw new BadRequestException(messageErreur);
		}
	}

	private void checkEmail(String email) {
		if (!email.matches("^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}[.][A-Za-z]{2,}$")) {
			String messageErreur = "L'email '" + email + "' n'est pas valide";
			LOGGER.warn(messageErreur);
			throw new BadRequestException(messageErreur);
		}
	}

	public void deletePersonne(Integer id) {
		LOGGER.info("Suppression de la personne - id='{}'", id);

		getPersonneById(id);

		PersonneFakeRepository.deletePersonne(id);
	}
}
