package com.goubinthibault.apiteststutorial.api.personne;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.goubinthibault.apiteststutorial.api.personne.PersonneEntity.SexeEnum;

public class PersonneFakeRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonneFakeRepository.class);

	public static Set<PersonneEntity> PERSONNES = new HashSet<>();

	private PersonneFakeRepository() {
	}

	public static Integer generateIdPersonne() {
		LOGGER.info("Generation d'un nouvel identifiant");
		List<PersonneEntity> personnesList = new ArrayList<>(PERSONNES);
		personnesList.sort(Comparator.comparing(PersonneEntity::getId));
		return (!personnesList.isEmpty()) ? personnesList.get(personnesList.size() - 1).getId() + 1 : 1;
	}

	public static void savePersonne(PersonneEntity personne) {
		LOGGER.info("Sauvegarde de la personne '{}'", personne);
		PERSONNES.removeIf(p -> p.getId() == personne.getId());
		PERSONNES.add(personne);
	}

	public static void deletePersonne(Integer id) {
		LOGGER.info("Suppression de la personne - id='{}'", id);
		PERSONNES.removeIf(p -> p.getId() == id);
	}

	public static List<PersonneEntity> searchPersonnes(String nom, String prenom, SexeEnum sexe) {
		LOGGER.info("Recherche de personnes : nom='{}', prenom='{}', sexe='{}'", nom, prenom, sexe);

		Stream<PersonneEntity> personneStream = PERSONNES.stream();

		if (nom != null && !"".equals(nom.trim())) {
			personneStream = personneStream.filter(p -> nom.equals(p.getNom()));
		}

		if (prenom != null && !"".equals(prenom.trim())) {
			personneStream = personneStream.filter(p -> prenom.equals(p.getPrenom()));
		}

		if (sexe != null) {
			personneStream = personneStream.filter(p -> sexe == p.getSexe());
		}

		return personneStream.collect(Collectors.toList());
	}

	public static PersonneEntity getPersonneById(Integer id) {
		LOGGER.info("Recuperation de la personne - id='{}'", id);

		Optional<PersonneEntity> personne = PERSONNES.stream().filter(p -> p.getId() == id).findFirst();

		if (personne.isPresent()) {
			return personne.get();
		}

		return null;
	}
}
